import os.path
import sqlite3
import re
from datetime import *
import itertools
import csv
import readline

class MyCompleter(object):  # Custom completer

    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options 
                                    if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]

        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None
 
if not os.path.isfile("comptes.db"):
    # Si la bdd n'existe pas on la crée
    comptes = sqlite3.connect("comptes.db")
    # Puis on crée les tables : Bières
    comptes.execute('''CREATE TABLE BIERES
    (ID	INT	NOT NULL,
    BIERE	TEXT	NOT NULL	UNIQUE);''')
    # Commandes
    comptes.execute('''CREATE TABLE COMMANDES
    (ID	INT	NOT NULL,
    DATE	TEXT,
    NOM	TEXT	NOT NULL,
    BIERE	TEXT	NOT NULL,
    QTE	        REAL	NOT NULL,
    LIVREE INT,
    PAYEE INT);''')
    # Commandes old
    comptes.execute('''CREATE TABLE COMMANDES_old
    (ID	INT	NOT NULL,
    DATE	TEXT,
    NOM	TEXT	NOT NULL,
    BIERE	TEXT	NOT NULL,
    QTE	        REAL	NOT NULL,
    LIVREE INT,
    PAYEE INT);''')
    comptes.execute('''CREATE TABLE SOUS
    (ID	INT	NOT NULL,
    DATE	TEXT,
    SOMME	REAL	NOT NULL,
    DESCRI	TEXT);''')
else:
    # Sinon on la lit
    comptes = sqlite3.connect("comptes.db")

def sql_add_biere(biere):
    cur = comptes.cursor()
    cur.execute(f'SELECT max(ID) FROM BIERES')
    id_biere = cur.fetchone()[0] + 1
    sql = f'INSERT INTO BIERES VALUES ({id_biere} , "{biere}")'
    cur.execute(sql)
    comptes.commit()
    return cur.lastrowid

def biere_from_id(id_biere):
    cur = comptes.cursor()
    try:
        cur.execute(f"SELECT BIERE from BIERES where ID = {id_biere}")
        biere = cur.fetchall()[0][0]
    except IndexError:
        return "wrong_entry"
    return biere
    
def sql_del_biere(biere):
    choix = input(f"Sur le point de supprimer la biere {biere} de toutes les commandes. \n v pour valider.\n q pour quitter et annuler.\n")
    if choix == "q":
        return "tomenu"
    elif choix == "v":
        #On supprime de la liste des bières en vente
        sql = f'DELETE from BIERES where BIERE = "{biere}"'
        cur = comptes.cursor()
        cur.execute(sql)
        # On ajoute les commandes à COMMANDES_old
        sql = f'INSERT into COMMANDES_old SELECT * from COMMANDES where BIERE = "{biere}"'
        cur.execute(sql)
        # On supprime les commandes de la table COMMANDES
        sql = f'DELETE from COMMANDES where BIERE = "{biere}"'
        cur.execute(sql)
        comptes.commit()
        return "done"
    else:
        return "wrong_entry"

def print_bieres(echo = True):
    cur = comptes.cursor()
    cur.execute(f"SELECT ID, BIERE from BIERES;")
    bieres = cur.fetchall()
    liste_bieres = [x[1] for x in bieres]
    if echo:
        print("Liste des bières en vente :")
        for biere in bieres:
            print(f"{biere[0]} : {biere[1]}")
        print("")
    return(liste_bieres)

def gestion_bieres():
    os.system("clear")
    print_bieres()
    biere = input("Entrez :\n Le nom d'une nouvelle bière pour l'ajouter à la liste.\n Le numéro d'une bière pour la supprimer de la liste et de toutes les commandes\n q pour quitter.\n")
    if biere == "q":
        return "tomenu_q"
    try:
        int(biere)
    except ValueError:    
        sql_add_biere(biere)
        return "done"
    nom_biere = biere_from_id(biere)
    if nom_biere == "wrong_entry":
        return "wrong_entry"
    else:
        del_out = sql_del_biere(nom_biere)
        return del_out

def sql_add_com(commande):
    sql = ''' INSERT INTO COMMANDES
              VALUES(?,?,?,?,?,?,?) '''
    cur = comptes.cursor()
    cur.execute(sql, commande)
    comptes.commit()
    return cur.lastrowid

def sql_update_com(ligne , commande):
    print(commande[2])
    sql = f'UPDATE COMMANDES SET DATE = "{commande[1]}", NOM = "{commande[2]}", BIERE = "{commande[3]}", QTE = {commande[4]}, LIVREE = {commande[5]}, PAYEE = {commande[6]} where ID = {ligne}'
    cur = comptes.cursor()
    cur.execute(sql)
    comptes.commit()
    return cur.lastrowid

def sql_del_com(com_id):
    choix = input(f"Sur le point de supprimer la commande {com_id}.\n v pour valider.\n q pour quitter et annuler.\n")
    if choix == "q":
        return "tomenu"
    elif choix == "v":
        sql = f'DELETE from COMMANDES where ID = {com_id}'
        cur = comptes.cursor()
        cur.execute(sql)
        comptes.commit()
        return "done"
    else:
        return "wrong_entry"
    
def prompt_commande(commande_dflt = None):
    os.system("clear")
    # On récupère la dernière commande si on n'en n'a pas choisi une par défaut
    if commande_dflt is None:
        cur = comptes.cursor()
        cur.execute("SELECT max(ID) from COMMANDES")
        id_max = cur.fetchone()[0]
        com_id = id_max + 1
        try:
            commande_dflt = [x for x in print_commandes(echo = False , gather = True) if x[0] == id_max][0]
        except IndexError:
            commande_dflt = ("NA" , "NA" , "NA" , "NA" , "NA" , "NA")
    else:
        com_id = commande_dflt[0]
    # On définit les valeurs par défaut
    date_dflt = commande_dflt[1]
    nom_dflt = commande_dflt[2]
    nom_biere_dflt = commande_dflt[3]
    qte_dflt = commande_dflt[4]
    livree_dflt = commande_dflt[5]
    payee_dflt = commande_dflt[6]
    # On définit les valeurs pour le completer
    commandes = print_commandes(echo = False)
    dates = [x[1] for x in commandes]
    noms = [x[2] for x in commandes]
    # Choix dans la date
    completer = MyCompleter(dates)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')
    date = input(f"Date de la livraison (par défaut : {date_dflt})\n")
    if date == "q":
        return "tomenu"
    elif date == "":
        date = date_dflt
    elif not re.match("\d{4}-\d{2}-\d{2}" , date):
        return "wrong_entry"
    # Nom
    completer = MyCompleter(noms)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')
    nom = input(f"Nom de la personne qui commande (par défaut : {nom_dflt})\n")
    if nom == "q":
        return "tomenu"
    elif nom == "":
        nom = nom_dflt
    # Choix de la bière
    print_bieres()
    id_biere = input(f"Entrez le numéro correspondant à la bière à commander (par défaut : {nom_biere_dflt})\n")
    if id_biere == "q":
        return "tomenu"
    elif id_biere == "":
        nom_biere = nom_biere_dflt
    else:
        try:
            int(id_biere)
            nom_biere = biere_from_id(id_biere)
            if nom_biere == "wrong_entry":
                return "wrong_entry"
        except ValueError:
            return "wrong_entry"
    # Quantité de bière commandée
    qte = input(f"Quantité de {nom_biere} commandée (par défaut : {qte_dflt})\n")
    if qte == "q":
        return "tomenu"
    elif qte == "":
        qte = qte_dflt
    else:
        try:
            float(qte)
        except ValueError:
            return "wrong_entry"
    livree = input(f"Commande livrée ? non = 0, oui = 1 (défaut : {livree_dflt}) : ")
    if livree == "":
        livree = livree_dflt
    elif livree not in ("0" , "1"):
        return wrong_entry
    else:
        livree = int(livree)
    payee = input(f"Commande payée ? non = 0, oui = 1 (défaut : {payee_dflt}) : ")
    if payee == "":
        payee = payee_dflt
    elif payee not in ("0" , "1"):
        return wrong_entry
    else:
        payee = int(payee)
    print(nom_biere)
    return (com_id , date , nom , nom_biere , qte , livree , payee)

def print_commandes(echo = True , gather = False):
    cur = comptes.cursor()
    cur.execute("SELECT ID, DATE, NOM, BIERE, QTE, LIVREE, PAYEE from COMMANDES")
    commandes = cur.fetchall()
    commandes.sort(key = lambda commande: commande[0])
    bieres = tuple(print_bieres(echo = False))
    if gather:
        if echo:
            header = ("Id" , "Date" , "Nom" , "Bière" , "Qté" , "L" , "P")
            format_row = "{:^4}" + "{:^12}" + "{:^20.20}" + "{:^16}" + "{:^6}" + "{:^4}" + "{:^4}"
            print(format_row.format(*header , ""))
            print("{:-<66}".format(""))
            for row in commandes:
                print(format_row.format(*row , ""))
        return commandes
    else:
        dates = list(set([x[1] for x in commandes]))
        commandes_table = []
        for date in dates:
            noms = list(set([x[2] for x in commandes if x[1] == date]))
            for nom in noms:
                coms_nom = [x for x in commandes if x[1] == date and x[2] == nom]
                com_nom = [coms_nom[0][0] , coms_nom[0][1] , coms_nom[0][2]]
                bieres_com = [x[3] for x in coms_nom]
                for biere in bieres:
                    if biere not in bieres_com:
                        qte_biere = [0]
                    else:
                        qte_biere = [x[4] for x in coms_nom if x[3] == biere]
                    com_nom = com_nom + qte_biere
                # Ajout du statut de livraison
                statut_livr = list(set([x[5] for x in coms_nom]))
                statut_paye = list(set([x[6] for x in coms_nom]))
                if len(statut_livr) > 1:
                    statut_livr = ["#"]
                if len(statut_paye) > 1:
                    statut_paye = ["#"]
                com_nom = com_nom + [statut_livr[0] , statut_paye[0]]
                commandes_table.append(com_nom)
        commandes_table = multisort(commandes_table , ((1,False) , (2,False)))
        if echo:
            print("Liste des commandes :")
            header = ("Date" , "Nom") + bieres + ("L" , "P")
            format_row = "{:^12}" + "{:^20.20}" + "{:^16}"*len(bieres) + "{:^4}" + "{:^4}"
            print(format_row.format(*header , ""))
            print("{:-<92}".format(""))
            for row in commandes_table:
                print(format_row.format(*row[1:] , ""))
        return commandes_table

def update_commande():
    os.system("clear")
    commandes = print_commandes(gather = True)
    com_id = input("Entrez :\n un numéro de ligne pour la modifier.\n sx pour supprimer la ligne x.\n l pour mettre à jour les statuts de livraison et paiement d'une livraison.\n q pour quitter.\n")
    if com_id == "q":
        return "tomenu"
    elif com_id == "l":
        dates = list(set([x[1] for x in commandes]))
        completer = MyCompleter(dates)
        readline.set_completer(completer.complete)
        readline.parse_and_bind('tab: complete')
        date = input(f"Date de la livraison : ")
        if not re.match("\d{4}-\d{2}-\d{2}" , date):
            return "wrong_entry"
        else:
            update_status(date)
            return "done_update"
    else:
        try:
            int(com_id)
        except ValueError:
            if re.match("s\d" , com_id):
                com_id = int(com_id.replace("s" , ""))
                del_com = sql_del_com(com_id)
                return del_com
            else:
                return "wrong_entry"
        commande = [x for x in commandes if x[0] == int(com_id)]
        commande_up = prompt_commande(commande_dflt = commande[0])
        print(commande_up)
        sql_update_com(com_id , commande_up)
        return "done_update"

def export_com(date = None , echo = True):
    commandes = print_commandes(echo = False)
    bieres = print_bieres(echo = False)
    if date is None:
        date = "All"
    else:
        commandes = [x for x in commandes if x[1] == date]
    commandes = [x[1:-2] for x in commandes]
    header = ["Date" , "Nom"] + bieres
    commandes.insert(0 , header)
    with open(f"{date}_commandes.csv" , "w" , newline = "") as f:
        writer = csv.writer(f)
        writer.writerows(commandes)
    return "done"
    
def gestion_commandes():
    os.system("clear")
    reID_commandes()
    commandes = print_commandes()
    print("")
    choix = input("Entrez :\n a pour ajouter une commande.\n m pour modifier une commande.\n e pour exporter les commandes en tableur.\n q pour quitter\n")
    if choix == "q":
        return "tomenu_q"
    elif choix == "a":
        commande = prompt_commande()
        if commande in ("tomenu" , "wrong_entry"):
            return commande
        sql_add_com(commande)
        return "done_com"
    elif choix == "m":
        up_status = update_commande()
        return up_status
    elif choix == "e":
        dates = [x[1] for x in commandes]
        completer = MyCompleter(dates)
        readline.set_completer(completer.complete)
        readline.parse_and_bind('tab: complete')
        date = input("Choisissez une date de commande. Par défaut, toutes\n")
        if date == "q":
            return "tomenu"
        elif date == "":
            export_com()
            return "done"
        elif re.match("\d{4}-\d{2}-\d{2}" , date):
            export_com(date)
            return "done"
        else:
            return "wrong_entry"
    else:
        return "wrong_entry"


def reID_entries():
    entries = print_entries(echo = False)
    entries = sorted(entries , key = lambda entry: entry[1])
    i = 1
    for entry in entries:
        date = entry[1]
        somme = entry[2]
        descri = entry[3]
        sql = f'UPDATE SOUS set ID = {i} where DATE = "{date}" AND SOMME = {somme} AND DESCRI = "{descri}"'
        cur = comptes.cursor()
        cur.execute(sql)
        i = i + 1
    comptes.commit()
    return

def sql_add_entry(entry_id , date , somme , descri):
    sql = f'INSERT INTO SOUS VALUES({entry_id} , "{date}" , {somme} , "{descri}")'
    cursor = comptes.cursor()
    cursor.execute(sql)
    comptes.commit()
    reID_entries()

def sql_del_entry(entry_id):
    sql = f'DELETE FROM SOUS WHERE ID = {entry_id}'
    cursor = comptes.cursor()
    cursor.execute(sql)
    comptes.commit()
    reID_entries()

def sql_up_entry(entry_id , date , somme , descri):
    sql = f'UPDATE SOUS SET DATE = "{date}", SOMME = {somme}, DESCRI = "{descri}" where ID = {entry_id}'
    cursor = comptes.cursor()
    cursor.execute(sql)
    comptes.commit()

def print_entries(echo = True):
    cursor = comptes.cursor()
    entries = cursor.execute(f'select ID, DATE, SOMME, DESCRI from SOUS')
    entries = sorted(entries , key = lambda entry: entry[0])
    if echo:
        print("Liste des entrées/sorties")
        header = ("ID" , "Date" , "Somme déposée" , "Description")
        format_row = "{:^5}" + "{:^15}" + "{:^15}" + "{:<35.35}"
        print(format_row.format(*header , ""))
        print("{:-<70}".format(""))
        for entry in entries:
            print(format_row.format(*entry , ""))
        print("")
    return entries

def add_entry():
    os.system("clear")
    print_entries()
    choix = input("Entrez:\n a pour ajouter une entrée.\n s pour supprimer une entrée.\n m pour modifier une entrée.\n q pour quitter.\n")
    if choix == "q":
        return "tomenu_q"
    elif choix == "a":
        cur = comptes.cursor()
        cur.execute("SELECT max(ID) from SOUS")
        id_max = cur.fetchone()[0]
        entry_id = id_max + 1
        try:
            entry_dflt = [x for x in print_entries(echo = False) if x[0] == id_max][0]
        except IndexError:
            entry_dflt = ("NA" , "NA" , "NA")
        date_dflt = datetime.now().date()
        somme_dflt = entry_dflt[2]
        date = input(f"Date (par défaut, {date_dflt}) : ")
        if date == "q":
            return "tomenu"
        elif date == "":
            date = date_dflt
        elif re.match("\d{4}-\d{2}-\d{2}" , date):
            date = date
        else:
            return "wrong_entry"
        somme = input("Somme à entrer : ")
        try:
            float(somme)
        except ValueError:
            return "wrong_entry"
        descri = input("Description : ")
        sql_add_entry(entry_id , date , somme , descri)            
        return "done_entry"
    elif choix == "s":
        entry_id = input("Entrez le numéro de la ligne à supprimer : ")
        try:
            int(entry_id)
        except ValueError:
            return "wrong_entry"
        sql_del_entry(int(entry_id))
        return "done_entry"
    elif choix == "m":
        entry_id = input("Entrez le numéro de la ligne à modifier : ")
        try:
            entry_id = int(entry_id)
        except ValueError:
            return "wrong_entry"
        entry_dflt = [x for x in print_entries(echo = False) if x[0] == entry_id][0]
        date_dflt = entry_dflt[1]
        somme_dflt = entry_dflt[2]
        descri_dflt = entry_dflt[3]
        date = input(f"Date (par défaut, {date_dflt}) : ")
        if date == "q":
            return "tomenu"
        elif date == "":
            date = date_dflt
        elif re.match("\d{4}-\d{2}-\d{2}" , date):
            date = date
        else:
            return "wrong_entry"
        default = f"(défaut : {somme_dflt})"
        somme = input(f"Somme à entrer {default} : ")
        if somme == "":
            somme = somme_dflt
        try:
            float(somme)
        except ValueError:
            return "wrong_entry"
        default = f"(défaut : {descri_dflt})"
        descri = input(f"Description {default} : ")
        if descri == "":
            descri = descri_dflt
        sql_up_entry(entry_id , date , somme , descri)
        return "done_entry"
    else:
        return "wrong_entry"

def as_date(str_date):
    str_date = f"{str_date} 14:00:00"
    date = datetime.strptime(str_date , "%Y-%m-%d %H:%M:%S")
    return date
    
def synthese():
    cursor = comptes.cursor()
    sql = f'SELECT BIERE, SUM(QTE) from COMMANDES where PAYEE = 0 GROUP BY BIERE;'
    cursor.execute(sql)
    commandes = cursor.fetchall()
    sql = f'SELECT BIERE, SUM(QTE) from COMMANDES where PAYEE = 1 GROUP BY BIERE;'
    cursor.execute(sql)
    ventes = cursor.fetchall()
    summary = []
    tot_com = 0
    tot_vend = 0
    for biere in print_bieres(echo = False):
        commande = [x for x in commandes if x[0] == biere]
        vente = [x for x in ventes if x[0] == biere]
        try:
            qte_com = commande[0][1]
        except IndexError:
            qte_com = 0
        try:
            qte_vend = vente[0][1]
        except IndexError:
            qte_vend = 0
        row = (biere , qte_vend , qte_vend*4 , qte_com , qte_com*4)
        summary.append(row)
        tot_com = tot_com + qte_com
        tot_vend = tot_vend + qte_vend
    total = ("Total :" , tot_vend , tot_vend * 4 , tot_com , tot_com * 4)
    header = ("Bière" , "Vol. vendu" , "Somme obtenue" , "Vol. commandé" , "Somme à venir")
    format_row = "{:^18.18}" + "{:^15}" * (len(header))
    print(format_row.format(*header , ""))
    print("{:-<80}".format(""))
    for row in summary:
        print(format_row.format(*row , ""))
    print("{:-<80}".format(""))
    print(format_row.format(*total , ""))
    print("")
    cursor = comptes.cursor()
    cursor.execute("select sum(SOMME) from SOUS")
    somme_dep = cursor.fetchone()[0]
    cursor.execute(f'select sum(QTE) from COMMANDES_old where PAYEE = 1')
    qte_vend_old = cursor.fetchone()[0]
    if somme_dep is None:
        somme_dep = 0
    if qte_vend_old is None:
        qte_vend_old = 0
    print(f"Somme déposée : {somme_dep}\nSomme obtenue total : {tot_vend*4 + qte_vend_old*4}\nReste à déposer : {tot_vend*4 + qte_vend_old*4 - somme_dep}")
    print("")

def multisort(xs, specs):
    for key, reverse in reversed(specs):
        xs.sort(key=lambda column: column[key], reverse=reverse)
    return xs

def reID_bieres():
    bieres = print_bieres(echo = False)
    bieres = sorted(bieres , key = lambda biere: biere[1])
    i = 1
    for biere in bieres:
        sql = f'UPDATE BIERES set ID = {i} where BIERE = "{biere}"'
        cur = comptes.cursor()
        cur.execute(sql)
        i = i + 1
    comptes.commit()
    return

def reID_commandes():
    commandes = print_commandes(echo = False , gather = True)
    commandes = multisort(commandes , ((1 , False) , (2 , False)))
    i = 1
    for commande in commandes:
        sql = f'UPDATE COMMANDES set ID = {i} where DATE = "{commande[1]}" AND NOM = "{commande[2]}" AND BIERE = "{commande[3]}" AND QTE = {commande[4]}'
        cur = comptes.cursor()
        cur.execute(sql)
        i = i + 1
    return
    
## Menu Principal
def update_status(date_vente):
    sql = f'UPDATE COMMANDES SET LIVREE = 1, PAYEE = 1 where DATE = "{date_vente}"'
    cursor = comptes.cursor()
    cursor.execute(sql)
    comptes.commit()
    return "done"

def menu(message = None):
    os.system("clear")
    reID_bieres()
    reID_commandes()
    if message is not None:
        print(message)
    else:
        print("Menu principal\n")
    synthese()
    choix = input("Entrez :\n b pour gérer la liste des bières en vente.\n c pour gérer les commandes.\n s pour gérer les entrées/sorties d'argent.\n q pour quitter\n")
    if choix == "q":
        return "quit"
    elif choix == "b":
        biere_out = gestion_bieres()
        return biere_out
    elif choix == "c":
        com_out = gestion_commandes()
        return com_out
    elif choix == "s":
        entry_out = add_entry()
        return entry_out
    else:
        return "wrong_entry"

choix_menu = menu()
while choix_menu != "quit":
    if choix_menu in ("done" , "tomenu_q"):
        choix_menu = menu()
    elif choix_menu == "tomenu":
        choix_menu = menu("Annulation. Retour au menu principal.\n")
    elif choix_menu == "wrong_entry":
        choix_menu = menu(message = "Mauvais paramètre entré. Retour au menu principal.\n")
    elif choix_menu in ("done_com" , "done_update"):
        choix_menu = gestion_commandes()
    elif choix_menu == "done_entry":
        choix_menu = add_entry()

comptes.close()
